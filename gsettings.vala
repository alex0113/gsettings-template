public static int main (string[] args) {
	try {
		// Custom location:

		string settings_dir = Path.get_dirname (args[0]);
		SettingsSchemaSource sss = new SettingsSchemaSource.from_directory (settings_dir, null, false);
		SettingsSchema schema = sss.lookup ("org.gnome.boxes.shared-folders", false);
		if (sss.lookup == null) {
			stdout.printf ("ID not found.");
			return 0;
		}

		Settings settings = new Settings.full (schema, null, null);


		/*
		Settings settings = new Settings("org.gnome.boxes.shared-folders");

                stdout.printf("Initial shared folders %s\n", shared_folders);
*/
		settings.changed["shared-folders"].connect (() => {
                        print("Added shared folder");
                        var shared_folders = settings.get_string("shared-folders");
                        print(shared_folders);

                        try{
                            //var variant_dict = Variant.parse(VariantType.VARDICT, "{'uuid': <'uuid'>, 'path': <'path'>}");
                            //print("ret"+variant_dict.print(true));
                            var variant = Variant.parse(new GLib.VariantType.array(GLib.VariantType.VARIANT), shared_folders);
                            print(variant.print (true));
                            VariantIter iter = variant.iterator ();
                            GLib.Variant? val = null;
                            print("\niter");

                            string password_str;
                            while (iter.next ("v",  &val)) {
			       stdout.printf ("Item  has type '%s'\n", val.get_type_string ());
			       val.lookup("path", "s", out password_str);
			       print("\n"+password_str);
			    }

                        } catch (VariantParseError e) {
                        	print("Message" + e.message);

                        	Variant.parse_error_print_context(e, "[<'uuid'>]");
                        }

                });


		//settings.set_int ("bottles-of-beer", bottles - 1);
		//settings.set_boolean ("lighting", !lighting);
		string shared_folders = settings.get_string("shared-folders");
		var dict_variant_builder = new GLib.VariantBuilder (GLib.VariantType.VARDICT);

		var uuid_variant = new GLib.Variant ("s", "uuid");

         	dict_variant_builder.add ("{sv}", "uuid", uuid_variant);
         	var path_variant = new GLib.Variant ("s", "path");
         	dict_variant_builder.add ("{sv}", "path", path_variant);
         	var credentials_variant = dict_variant_builder.end ();

		var dict_variant_builder2 = new GLib.VariantBuilder (GLib.VariantType.VARDICT);

		var uuid_variant2 = new GLib.Variant ("s", "uuid2");
         	dict_variant_builder2.add ("{sv}", "uuid", uuid_variant2);
         	var path_variant2 = new GLib.Variant ("s", "path2");
         	dict_variant_builder2.add ("{sv}", "path", path_variant2);
         	var credentials_variant2 = dict_variant_builder2.end ();

		var array_variant_builder = new GLib.VariantBuilder (new GLib.VariantType.array(VariantType.VARIANT));
		print("cred is array "+ credentials_variant.get_type().dup_string ());
		array_variant_builder.add("v",  credentials_variant);
		array_variant_builder.add("v", credentials_variant2);
		print("here32");
		var array_variant = array_variant_builder.end ();
		string array_str = array_variant.print (true);
		print("array" + array_str);
		//const string constar[] = {"first", "second", "third", (owned)credentials_str, null};
		settings.set_string ("shared-folders", array_str);
		print(array_variant.check_format_string("a*", true)==true?"da":"nu");

		stdout.puts ("\nPlease start 'dconf-editor' and edit keys in /org/example/my-app/\n");
		new MainLoop ().run ();
	} catch (Error e) {
		stdout.printf ("Error: %s\n", e.message);
	}
	return 0;
}
